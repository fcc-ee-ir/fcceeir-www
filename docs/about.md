# About this page

This page was created following the instructions found in [Documentation How-to Guide](https://how-to.docs.cern.ch/new/green/)

Further information on how to edit the top [mkdocs.yml](https://gitlab.cern.ch/efthymio/iedocs) file can be found [here](https://www.mkdocs.org/user-guide/configuration/)


