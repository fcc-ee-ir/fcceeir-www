# FCC-ee Interaction Region Docs Web

In this site we will collect documentation, tools and other information related to FCC-ee Interaction Region design and studies.

The goal is to share information, keep track of updates and provide tools and hints for the studies. 

It is organized as follows:

- **CAD** : CAD files for the IR region 
- **Software** : software packages and tools
- **How-tos** : 


## Useful information

### Meetings
- [MDI meetings](https://indico.cern.ch/category/5665/)
- [MDI Detector meetings](https://indico.cern.ch/category/8608/)  
- [FCCee optics meeting](https://indico.cern.ch/event/1014448/)
- [FCCee Physics performance meeting](https://indico.cern.ch/category/12894/)

### Documents
- FCCee [CDR](http://cds.cern.ch/record/2651299?ln=en)
